package ru.highflyer.rcw;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.util.Log;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

/**
 * Created with IntelliJ IDEA.
 * User: alexey.nikitin
 * Date: 25.06.14 18:41
 */
final public class PhoneStateBroadcastReceiver extends BroadcastReceiver {
    private static final String TAG = PhoneStateBroadcastReceiver.class.getSimpleName();

    @Override
    public void onReceive( Context context, Intent intent ) {
        Log.d(TAG, "onReceive");
        context = context.getApplicationContext();

        Class<RecentlyCallsWidget> clazz = RecentlyCallsWidget.class;

        Intent updateIntent = new Intent(context, clazz);
        updateIntent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        ComponentName componentName = new ComponentName(context, clazz);

        int [] appWidgetIds = AppWidgetManager.getInstance(context).getAppWidgetIds(componentName);
        if (appWidgetIds == null || appWidgetIds.length <= 0) {
            Log.d(TAG, "No one widget :(");
            return;
        }

        Log.d(TAG, String.format("Widget ids: %s", Arrays.toString(appWidgetIds)));
        updateIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetIds);

        AlarmManager am = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);

        am.set(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime() + 1000,
               PendingIntent.getBroadcast(context,
               ((int)(System.currentTimeMillis() % TimeUnit.DAYS.toMillis(1))),
               updateIntent, PendingIntent.FLAG_ONE_SHOT));
    }
}
