package ru.highflyer.rcw;

import android.app.Application;
import org.acra.ACRA;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

/**
 * Created by alexey.nikitin on 29.07.14.
 */
@ReportsCrashes(
                mailTo = "aleksvas@gmail.com",
                mode = ReportingInteractionMode.SILENT)
public final class AppEx extends Application {
    @Override
    public void onCreate( ) {
        super.onCreate();

        ACRA.init(this);
    }
}
