package ru.highflyer.rcw;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: alexey.nikitin
 * Date: 26.06.14 10:52
 */
final public class DataFetcher {
    private static final String TAG = DataFetcher.class.getSimpleName();

    private static final String [] CONTACT_ID_PROJECTION =
            new String [] {ContactsContract.PhoneLookup._ID};

    public static List<CallEntry> readCallLog( Context context, int maxEntries ) {
        Log.d(TAG, "readCallLog");

        String columns [] = new String [] {
                CallLog.Calls.NUMBER,
                CallLog.Calls.TYPE,
                CallLog.Calls.CACHED_NAME};

        List<CallEntry> result = new ArrayList<>();

        Cursor c = context.getContentResolver().query(CallLog.Calls.CONTENT_URI,
                columns, null, null, CallLog.Calls.DATE + " DESC");

        if (c == null) {
            return result;
        }

        try {
            Set<Long> contactIds = new HashSet<>();
            Set<String> phoneNumbers = new HashSet<>();

            while (c.moveToNext()) {
                String number = c.getString(0);
                int type = c.getInt(1);
                String cachedName = c.getString(2);

                if (TextUtils.isEmpty(number))
                    continue;

                if (phoneNumbers.contains(number))
                    continue;

                phoneNumbers.add(number);

                Bitmap bitmap = null;
                long contactId = fetchContactIdFromPhoneNumber(context, number);
                if (contactId != 0) {
                    if (contactIds.contains(contactId))
                        continue;

                    contactIds.add(contactId);
                    bitmap = getPhotoBitmap(context, contactId);
                }

                CallEntry callEntry = new CallEntry(number, CallEntry.Type.byContentProviderValue(type), cachedName, bitmap);
                Log.d(TAG, String.format("Log entry added: %s", callEntry));
                result.add(callEntry);
                if (result.size() >= maxEntries) {
                    Log.d(TAG, String.format("Max call log entries reached: %d", maxEntries));
                    break;
                }
            }
        } finally {
            c.close();
        }

        return result;
    }

    private static long fetchContactIdFromPhoneNumber( Context context, String phoneNumber ) {
        Uri uri = Uri.withAppendedPath(
                ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));

        Cursor cursor = context.getContentResolver().query(uri, CONTACT_ID_PROJECTION, null, null, null);

        try {
            if (cursor.moveToFirst())
                return cursor.getLong(cursor.getColumnIndex(ContactsContract.PhoneLookup._ID));
        } finally {
            cursor.close();
        }

        return 0;
    }

    private static Bitmap getPhotoBitmap( Context context, long contactId ) {
        Uri person = ContentUris.withAppendedId(
                ContactsContract.Contacts.CONTENT_URI, contactId);
        InputStream photoStream = ContactsContract.Contacts.openContactPhotoInputStream(
                context.getContentResolver(), person, true);
        if (photoStream == null)
            return null;

        int size = context.getResources().getDimensionPixelSize(R.dimen.avatar_size);

        try {
            BufferedInputStream buf = new BufferedInputStream(photoStream);
            // First decode with inJustDecodeBounds=true to check dimensions
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(buf, null, options);
            buf.close();

            buf = new BufferedInputStream(ContactsContract.Contacts.openContactPhotoInputStream(
                    context.getContentResolver(), person, true));
            options.inSampleSize = calculateInSampleSize(options, size, size);

            // Decode bitmap with inSampleSize set
            options.inJustDecodeBounds = false;
            return BitmapFactory.decodeStream(buf, null, options);
        } catch (Exception e) {
            Log.e(TAG, String.format("Failed to fetch contact photo: %d", contactId), e);
            return null;
        }
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }
}
