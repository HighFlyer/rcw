package ru.highflyer.rcw;

import android.app.Activity;
import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;

/**
 * Created by alexey.nikitin on 10.11.15.
 */
public final class ConfigureAppWidget extends Activity implements CompoundButton.OnCheckedChangeListener {
    static final String EXTRA_AUTO_SIZE = "auto-size";
    static final String EXTRA_ROWS = "rows";
    static final String EXTRA_COLUMNS = "columns";
    private int appWidgetId;

    private EditText rows;
    private EditText columns;
    private Switch autoColumns;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            appWidgetId = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
        }

        setResult(RESULT_CANCELED);

        setContentView(R.layout.preferences);
        rows = (EditText)findViewById(R.id.rows);
        columns = (EditText)findViewById(R.id.columns);
        autoColumns = (Switch)findViewById(R.id.auto_columns);
        autoColumns.setOnCheckedChangeListener(this);
    }

    public void onAddClicked(View view) {
        boolean auto = autoColumns.isChecked();

        int rowI = 2;
        try {
            rowI = Integer.parseInt(rows.getText().toString());
        } catch (Exception e) {
        }
        int colI = 5;
        try {
            colI = Integer.parseInt(columns.getText().toString());
        } catch (Exception e) {
        }

        PreferenceManager.getDefaultSharedPreferences(this).edit().
                putBoolean(EXTRA_AUTO_SIZE, auto).
                putInt(EXTRA_ROWS, rowI).
                putInt(EXTRA_COLUMNS, colI).
                apply();

        Intent resultValue = new Intent();

        resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
        setResult(RESULT_OK, resultValue);

        finish();
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        rows.setEnabled(!isChecked);
        columns.setEnabled(!isChecked);
    }
}
