package ru.highflyer.rcw;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;

import java.util.List;

public final class RecentlyCallsWidget extends AppWidgetProvider {
    private static final String TAG = RecentlyCallsWidget.class.getSimpleName();

    @Override
    public void onUpdate( Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds ) {
        // There may be multiple widgets active, so update all of them
        for (int appWidgetId : appWidgetIds)
            updateAppWidget(context, appWidgetManager, appWidgetId);
    }

    @Override
    public void onEnabled( Context context ) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled( Context context ) {
        // Enter relevant functionality for when the last widget is disabled
    }

    @Override
    public void onAppWidgetOptionsChanged( Context context, AppWidgetManager appWidgetManager, int appWidgetId,
                                           Bundle newOptions ) {
        super.onAppWidgetOptionsChanged(context, appWidgetManager, appWidgetId, newOptions);
        updateAppWidget(context, appWidgetManager, appWidgetId);
    }

    static void updateAppWidget( Context context, AppWidgetManager appWidgetManager, int appWidgetId ) {
        Log.d(TAG, "updateAppWidget");
        if (appWidgetManager == null)
            return;

        Bundle appWidgetOptions = appWidgetManager.getAppWidgetOptions(appWidgetId);
        if (appWidgetOptions == null)
            return;

        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        boolean autoColumns = prefs.getBoolean(ConfigureAppWidget.EXTRA_AUTO_SIZE, true);
        int columns, rows;
        if (autoColumns) {
            int minWidth = appWidgetOptions.getInt(AppWidgetManager.OPTION_APPWIDGET_MIN_WIDTH, 0);
            int minHeight = appWidgetOptions.getInt(AppWidgetManager.OPTION_APPWIDGET_MIN_HEIGHT, 0);

            columns = minWidth / 72;
            rows = minHeight / 72;

            Log.d(TAG, String.format("Sizes: %d, %d, c: %d, r: %d", minWidth, minHeight, columns, rows));
        } else {
            columns = prefs.getInt(ConfigureAppWidget.EXTRA_COLUMNS, 1);
            rows = prefs.getInt(ConfigureAppWidget.EXTRA_ROWS, 1);

            Log.d(TAG, String.format("Columns: %d, rows: %d", columns, rows));
        }

        columns = Math.max(columns, 1);
        rows = Math.max(rows, 1);

        RemoteViews verticalList = new RemoteViews(context.getPackageName(), R.layout.vertical_list);
        verticalList.removeAllViews(R.id.vertical_list);

        List<CallEntry> entries = DataFetcher.readCallLog(context, rows * columns);
        if (entries.isEmpty()) {
            RemoteViews empty = new RemoteViews(context.getPackageName(), R.layout.empty_calllog);
            appWidgetManager.updateAppWidget(appWidgetId, empty);
            return;
        }

        int entryIndex = 0;

        for (int r = 0; r < rows && entryIndex < entries.size(); r++) {
            RemoteViews horizontalList = new RemoteViews(context.getPackageName(), R.layout.horizontal_list);
            horizontalList.removeAllViews(R.layout.horizontal_list);

            verticalList.addView(R.id.vertical_list, horizontalList);

            for (int c = 0; c < columns && entryIndex < entries.size(); c++, entryIndex++) {
                CallEntry entry = entries.get(entryIndex);

                RemoteViews cell = new RemoteViews(context.getPackageName(), R.layout.call_item);
                horizontalList.addView(R.id.horizontal_list, cell);

                CallEntry.Type type = entry.type;
                if (type != null)
                    cell.setImageViewResource(R.id.icon, type.getIconResourceId());
                else
                    cell.setImageViewBitmap(R.id.icon, null);

                cell.setOnClickPendingIntent(R.id.cell_container, createPendingIntent(context, entry));

                if (entry.photo == null) {
                    cell.setViewVisibility(R.id.name, View.VISIBLE);
                    cell.setTextViewText(R.id.name, entry.getText());
                } else {
                    cell.setViewVisibility(R.id.name, View.GONE);
                    cell.setImageViewBitmap(R.id.avatar, entry.photo);
                }
            }
        }

        appWidgetManager.updateAppWidget(appWidgetId, verticalList);
    }

    private static PendingIntent createPendingIntent( Context context, CallEntry entry ) {
        Uri uri = Uri.parse("tel:" + entry.number);
        Log.d(TAG, String.format("Pending intent uri: '%s' for entry %s", uri, entry));

        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(uri);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        return PendingIntent.getActivity(context.getApplicationContext(),
                entry.hashCode(), intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }
}


