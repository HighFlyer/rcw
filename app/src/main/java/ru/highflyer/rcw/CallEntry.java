package ru.highflyer.rcw;

import android.graphics.Bitmap;
import android.provider.CallLog;
import android.text.TextUtils;

/**
 * Created with IntelliJ IDEA.
 * User: alexey.nikitin
 * Date: 25.06.14 17:20
 */
final public class CallEntry {
    public enum Type {
        MISSED(CallLog.Calls.MISSED_TYPE, R.drawable.call_missed),
        INCOMING(CallLog.Calls.INCOMING_TYPE, R.drawable.call_incoming),
        OUTGOING(CallLog.Calls.OUTGOING_TYPE, R.drawable.call_outgoing);

        private final int contentProviderValue;
        private final int iconResourceId;

        Type( int contentProviderValue, int iconResourceId ) {
            this.contentProviderValue = contentProviderValue;
            this.iconResourceId = iconResourceId;
        }

        public int getIconResourceId( ) {
            return iconResourceId;
        }

        public static Type byContentProviderValue( int type ) {
            for (Type t : values())
                if (t.contentProviderValue == type)
                    return t;

            return null;
        }
    }

    public final String number;
    public final Type type;
    public final String name;
    public final Bitmap photo;

    public CallEntry( String number, Type type, String name, Bitmap photo ) {
        this.number = number;
        this.type = type;
        this.name = name;
        this.photo = photo;
    }

    public CharSequence getText( ) {
        return !TextUtils.isEmpty(name) ? name : number;
    }

    @Override
    public boolean equals( Object o ) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CallEntry entry = (CallEntry)o;

        if (name != null ? !name.equals(entry.name) : entry.name != null) return false;
        if (number != null ? !number.equals(entry.number) : entry.number != null) return false;
        if (type != entry.type) return false;

        return true;
    }

    @Override
    public int hashCode( ) {
        int result = number != null ? number.hashCode() : 0;
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @Override
    public String toString( ) {
        return "CallEntry{" +
                "number='" + number + '\'' +
                ", type=" + type +
                ", name='" + name + '\'' +
                ", photoUri=" + photo +
                '}';
    }
}
